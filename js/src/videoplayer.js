(function () {
	'use strict';

	// Does the browser actually support the video element?
	var supportsVideo = !!document.createElement('video').canPlayType;

	if (supportsVideo) {
		// Obtain handles to main elements
		var videoContainer = document.getElementById('videoContainer');
		var video = document.getElementById('video');
		var videoControls = document.getElementById('video-controls');

		// Hide the default controls
		video.controls = false;

		// Display the user defined video controls
		videoControls.setAttribute('data-state', 'visible');

		// Obtain handles to buttons and other elements
		var playpause = document.getElementById('playpause');
		var stop = document.getElementById('stop');
		var mute = document.getElementById('mute');
		var volinc = document.getElementById('volinc');
		var voldec = document.getElementById('voldec');
		var brogress = document.getElementById('brogress');
		var brogressBar = document.getElementById('brogress-bar');
		//var fullscreen = document.getElementById('fs');
		var subtitles = document.getElementById('subtitles');

		// If the browser doesn't support the brogress element, set its state for some different styling
		var supportsbrogress = (document.createElement('brogress').max !== undefined);
		if (!supportsbrogress) brogress.setAttribute('data-state', 'fake');
		/*
		// Check if the browser supports the Fullscreen API
		var fullScreenEnabled = !!(document.fullscreenEnabled || document.mozFullScreenEnabled || document.msFullscreenEnabled || document.webkitSupportsFullscreen || document.webkitFullscreenEnabled || document.createElement('video').webkitRequestFullScreen);
		// If the browser doesn't support the Fulscreen API then hide the fullscreen button
		if (!fullScreenEnabled) {
			fullscreen.style.display = 'none';
		}
		*/

		// Check the volume
		var checkVolume = function(dir) {
			if (dir) {
				var currentVolume = Math.floor(video.volume * 10) / 10;
				if (dir === '+') {
					if (currentVolume < 1) video.volume += 0.1;
				}
				else if (dir === '-') {
					if (currentVolume > 0) video.volume -= 0.1;
				}
				// If the volume has been turned off, also set it as muted
				// Note: can only do this with the custom control set as when the 'volumechange' event is raised, there is no way to know if it was via a volume or a mute change
				if (currentVolume <= 0) video.muted = true;
				else video.muted = false;
			}
			changeButtonState('mute');
		}

		// Change the volume
		var alterVolume = function(dir) {
			checkVolume(dir);
		}
		/*
		// Set the video container's fullscreen state
		var setFullscreenData = function(state) {
			videoContainer.setAttribute('data-fullscreen', !!state);
			// Set the fullscreen button's 'data-state' which allows the correct button image to be set via CSS
			fullscreen.setAttribute('data-state', !!state ? 'cancel-fullscreen' : 'go-fullscreen');
		}

		// Checks if the document is currently in fullscreen mode
		var isFullScreen = function() {
			return !!(document.fullScreen || document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement || document.fullscreenElement);
		}
		*/
		// Fullscreen
		/*
		var handleFullscreen = function() {
			// If fullscreen mode is active...	
			if (isFullScreen()) {
					// ...exit fullscreen mode
					// (Note: this can only be called on document)
					if (document.exitFullscreen) document.exitFullscreen();
					else if (document.mozCancelFullScreen) document.mozCancelFullScreen();
					else if (document.webkitCancelFullScreen) document.webkitCancelFullScreen();
					else if (document.msExitFullscreen) document.msExitFullscreen();
					setFullscreenData(false);
				}
				else {
					// ...otherwise enter fullscreen mode
					// (Note: can be called on document, but here the specific element is used as it will also ensure that the element's children, e.g. the custom controls, go fullscreen also)
					if (videoContainer.requestFullscreen) videoContainer.requestFullscreen();
					else if (videoContainer.mozRequestFullScreen) videoContainer.mozRequestFullScreen();
					else if (videoContainer.webkitRequestFullScreen) {
						// Safari 5.1 only allows proper fullscreen on the video element. This also works fine on other WebKit browsers as the following CSS (set in styles.css) hides the default controls that appear again, and 
						// ensures that our custom controls are visible:
						// figure[data-fullscreen=true] video::-webkit-media-controls { display:none !important; }
						// figure[data-fullscreen=true] .controls { z-index:2147483647; }
						video.webkitRequestFullScreen();
					}
					else if (videoContainer.msRequestFullscreen) videoContainer.msRequestFullscreen();
					setFullscreenData(true);
				}
			}*/

		// Only add the events if addEventListener is supported (IE8 and less don't support it, but that will use Flash anyway)
		if (document.addEventListener) {
			// Wait for the video's meta data to be loaded, then set the brogress bar's max value to the duration of the video
			video.addEventListener('loadedmetadata', function() {
				brogress.setAttribute('max', video.duration);
			});

			// Changes the button state of certain button's so the correct visuals can be displayed with CSS
			var changeButtonState = function(type) {
				// Play/Pause button
				if (type == 'playpause') {
					if (video.paused || video.ended) {
						playpause.setAttribute('data-state', 'play');
					}
					else {
						playpause.setAttribute('data-state', 'pause');
					}
				}
				// Mute button
				else if (type == 'mute') {
					mute.setAttribute('data-state', video.muted ? 'unmute' : 'mute');
				}
			}

			// Add event listeners for video specific events
			video.addEventListener('play', function() {
				changeButtonState('playpause');
			}, false);
			video.addEventListener('pause', function() {
				changeButtonState('playpause');
			}, false);
			video.addEventListener('volumechange', function() {
				checkVolume();
			}, false);

			// Add events for all buttons			
			playpause.addEventListener('click', function(e) {
				if (video.paused || video.ended) video.play();
				else video.pause();
			});	

			// Turn off all subtitles
			for (var i = 0; i < video.textTracks.length; i++) {
				video.textTracks[i].mode = 'hidden';
			}

			// Creamos un botón del menu de subtitulos
			var subtitleMenuButtons = [];//en este array iremos guardando los botones que vayamos creando
			var createMenuItem = function(id, lang, label) {
				var listItem = document.createElement('li'); //creamos un elemento de lista
				var button = listItem.appendChild(document.createElement('button'));//le asignamos un botón
				button.setAttribute('id', id);//definimos el identificador
				button.className = 'subtitles-button';//el nombre
				if (lang.length > 0) button.setAttribute('lang', lang);//el lenguaje
				button.value = label;//la etiqueta
				button.setAttribute('data-state', 'inactive');//y ponemos el estado a inactivo
				button.appendChild(document.createTextNode(label)); //y el texto que aparecerá en el boton
				button.addEventListener('click', function(e) {//definimos el comportamiendo al clicar en el botón
					// Ponemos los botones inactivos para evitar tener varios activos a la vez
					subtitleMenuButtons.map(function(v, i, a) {//con la función map podemos poner todos en inactivo sin necesidad de hacer
															  //de hacer un bucle
						subtitleMenuButtons[i].setAttribute('data-state', 'inactive');
					});
					// Buscamos el leguaje que debe activarse
					var lang = this.getAttribute('lang');
					for (var i = 0; i < video.textTracks.length; i++) {
						if (video.textTracks[i].language == lang) {//cuando lo encontramos, le poenmos el modo a showing
							video.textTracks[i].mode = 'showing';
							this.setAttribute('data-state', 'active');//y ponemos su estado en activo
						}
						else { //para el botón de apagado de los subtitulos, el campo lang es vacio, asi que no encontrará ningun resultado
							//y pondra todos los textTracks a hidden
							video.textTracks[i].mode = 'hidden';
						}
					}
					subtitlesMenu.style.display = 'none'; //hacer que el menú se repliegue
				});
				subtitleMenuButtons.push(button);//añadimos el botón a la array que contiene los botones
				return listItem;
			}
			//Creamos un menú desplegable con las opciones de subtítulos
			var subtitlesMenu;
			if (video.textTracks) {//si el vídeo tiene textTracks, creamos el menu
				var df = document.createDocumentFragment();//crea documento imaginario
				var subtitlesMenu = df.appendChild(document.createElement('ul'));//creamos una lista
				subtitlesMenu.className = 'subtitles-menu';
				subtitlesMenu.appendChild(createMenuItem('subtitles-off', '', 'Off'));//creamos el botón para apagar los subtítulos
				for (var i = 0; i < video.textTracks.length; i++) {//por cada textTrack, creamos un botón
					subtitlesMenu.appendChild(createMenuItem('subtitles-' + video.textTracks[i].language, video.textTracks[i].language, video.textTracks[i].label));
				}
				videoContainer.appendChild(subtitlesMenu);//añadimos el menú
			}
			subtitles.addEventListener('click', function(e) {
				if (subtitlesMenu) {
					subtitlesMenu.style.display = (subtitlesMenu.style.display == 'block' ? 'none' : 'block');
				}
			});

			// The Media API has no 'stop()' function, so pause the video and reset its time and the brogress bar
			stop.addEventListener('click', function(e) {
				video.pause();
				video.currentTime = 0;
				brogress.value = 0;
				// Update the play/pause button's 'data-state' which allows the correct button image to be set via CSS
				changeButtonState('playpause');
			});
			mute.addEventListener('click', function(e) {
				video.muted = !video.muted;
				changeButtonState('mute');
			});
			volinc.addEventListener('click', function(e) {
				alterVolume('+');
			});
			voldec.addEventListener('click', function(e) {
				alterVolume('-');
			});
			/*fs.addEventListener('click', function(e) {
				handleFullscreen();
			});*/

			// As the video is playing, update the brogress bar
			video.addEventListener('timeupdate', function() {
				// For mobile browsers, ensure that the brogress element's max attribute is set
				if (!brogress.getAttribute('max')) brogress.setAttribute('max', video.duration);
				brogress.value = video.currentTime;
				brogressBar.style.width = Math.floor((video.currentTime / video.duration) * 100) + '%';
			});

			// React to the user clicking within the brogress bar
			brogress.addEventListener('click', function(e) {
				// Also need to take the parents into account here as .controls and figure now have position:relative
				var pos = (e.pageX  - (this.offsetLeft + this.offsetParent.offsetLeft + this.offsetParent.offsetParent.offsetLeft)) / this.offsetWidth;
				video.currentTime = pos * video.duration;
			});

			// Listen for fullscreen change events (from other controls, e.g. right clicking on the video itself)
			/*
			document.addEventListener('fullscreenchange', function(e) {
				setFullscreenData(!!(document.fullScreen || document.fullscreenElement));
			});
			document.addEventListener('webkitfullscreenchange', function() {
				setFullscreenData(!!document.webkitIsFullScreen);
			});
			document.addEventListener('mozfullscreenchange', function() {
				setFullscreenData(!!document.mozFullScreen);
			});
			document.addEventListener('msfullscreenchange', function() {
				setFullscreenData(!!document.msFullscreenElement);
			});*/

			$.getJSON("./vtt/turns.json", function(json) {//obtenemos el JSON con jQuery
				var turns = json;
				var turnslength = Object.keys(turns).length; //obtenemos el número de objetos en el fichero contando los 
				//objetos turn con la funcion Object.keys
				console.log(Object.keys(turns).length);
				for (var i = 0; i < turnslength; i++){ //iteramos sobre el número de objetos
				 var idbutton='#Turn'+i;//identificador del botón
				 var timeturn=turns["Turn"+i].Time;//instante de tiempo al que mover el puntero del vídeo
				 //creamos y añadimos los botones a la lista usando jQuery
				$('#tableturns > tbody').append('<tr class="tablebackground"><td><button class="turntable" id="Turn'+i+'" type="button">'+turns["Turn"+i].Title+'</button></td></tr>');
				setCurTime(timeturn,idbutton)	
				}	
			});

			
			//En esta función cambiaremos la propiedad currentTime del vídeo al tiempo que hemos especificado en el fichero JSON	
			function setCurTime(timeturn,idbutton) {
					var timedit = timeturn;
					var myidbutton = idbutton;
				$(myidbutton).click(function() {
					video.currentTime = timedit;
				});	
			}


			window.addEventListener("load", function() {
			// CHROME
			if(adapt == true){
			if (navigator.userAgent.indexOf("Chrome") != -1 ) {
				console.log("Google Chrome");
				//$("#divVideo video")[0].load();
				$('#video').attr('src','./video/high_dash.mpd');
			}
			// FIREFOX
			else if (navigator.userAgent.indexOf("Firefox") != -1 ) {
				console.log("Mozilla Firefox");
				$('#video').attr('src','./video/high_dash.mpd');
			}
			// INTERNET EXPLORER
			else if (navigator.userAgent.indexOf("MSIE") != -1 ) {
				console.log("Internet Exploder");
				$('#video').attr('src','./video/high_dash.mpd');
			}
			// EDGE
			else if (navigator.userAgent.indexOf("Edge") != -1 ) {
				console.log("Internet Exploder");
				$('#video').attr('src','./video/high_dash.mpd');
			}
			// SAFARI
			else if (navigator.userAgent.indexOf("Safari") != -1 ) {
				console.log("Safari");
				$('#video').attr('src','./video/hls/hls.m3u8');
			}
			// OPERA
			else if (navigator.userAgent.indexOf("Opera") != -1 ) {
				console.log("Opera");
				$('#video').attr('src','./video/high_dash.mpd');
			}
			// YANDEX BROWSER
			else if (navigator.userAgent.indexOf("Opera") != -1 ) {
				console.log("YaBrowser");
				$('#video').attr('src','./video/high_dash.mpd');
			}
		
			// OTHERS
			else {
				console.log("Others");
				$('#video').attr('src','./video/high_dash.mpd');
			}
		}
		if (adapt == false){
			console.log("Adaptative disabled");
		}
});
		}
	 }

 })();