// Tratamiendo de cues 
if(adapt==true)
{
  $('#foradapt').append(
    '<video id="video" data-dashjs-player autoplay controls preload="metadata" autobuffer>'+
    '<track src="./vtt/game1.vtt" kind="metadata" default>'+	
    '<track src="./vtt/vampyrsubtitles.vtt" label="Español" kind="subtitles" srclang="es" >'+
    '<track src="./vtt/vampyrpunctuation.vtt" kind="metadata" >'+
      '</video>');
}
else{
  $("#foradapt").append(
    '<video id="video" src="./video/Game1Music.mp4" controls preload="metadata" autobuffer>'+
    '<track src="./vtt/game1.vtt" kind="metadata" default>'+	
    '<track src="./vtt/vampyrsubtitles.vtt" label="Español" kind="subtitles" srclang="es" >'+
    '<track src="./vtt/vampyrpunctuation.vtt" kind="metadata" >'+
    '</video>');
}
var videoElement = document.querySelector("video");//cogemos el vídeo
var textTracks = videoElement.textTracks; // sacamos las pistas de texto del vídeo (pueden ser varios subtítulos, metadatos, etc)
var textTrackimages = textTracks[0]; //suponemos que el elemento 0 del array es el vtt para cargar las cartas
var textTrackpunctuation = textTracks[2]; //suponemos que el elemento 2 del array es el vtt para cargar la puntuación
//var cuespunctuation = textTrackpunctuation.cues;//¿?
//var cuesimages = textTrackimages.cues;//¿?

textTrackimages.oncuechange = function (){
    // "this" is a textTrack
    var cue = this.activeCues[0]; // sólo tenemos una cue activa 
    var obj = JSON.parse(cue.text);// sacamos el texto de la cue y lo transformamos a JSON

    //por cada atributo del JSON comprobamos si está vacío o no
    //si no lo está, cargamos la imagen que nos indica el contenido de cada atributo
    if(obj.Vampiro1 != "") $("#vampiro1").attr("src",obj.Vampiro1);
    if(obj.Vampiro2 != "") $("#vampiro2").attr("src",obj.Vampiro2);
    if(obj.Carta1 != "") $("#carta1").attr("src",obj.Carta1);
    if(obj.Carta2 != "") $("#carta2").attr("src",obj.Carta2);

  }

  textTrackpunctuation.oncuechange = function (){
    // "this" is a textTrack
    var cue = this.activeCues[0]; // sólo tenemos una cue activa
    var obj = JSON.parse(cue.text); // sacamos el texto de la cue y lo transformamos a JSON
    $('#punctuationTable > tbody').html(''); //Vacía la tabla (asigna el valor vacío al cuerpo de la tabla)  
    for (var i = 0; i < obj.Players.length; i++){//creamos filas de la tabla con la puntuación actualizada
    $('#punctuationTable > tbody').append('<tr class="playertable tablebackground"> <td>'+obj.PlayersName[i]+'</td><td>'+obj.Players[i]+'</td></tr>'); 
    }
    
  }


/*
for (var i = 0; i < cuesimages.length; i++){
    cue = cuesimages[i];
    cue.onenter = function(){
    $("#vampiro1").attr("src",);
    $("#vampiro2").attr("src",);
    //$("#carta1").attr("src","./img/vampiro 2.jpg");
    //$("#carta2").attr("src","./img/vampiro 2.jpg");

        //alert("HOSTIA");
        //coger el JSON
        //Obtener el nombre de la carta y cargarla
    }
}*/